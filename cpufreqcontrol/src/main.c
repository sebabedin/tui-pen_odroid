/*
 ============================================================================
 Name        : vielearbeit.c
 Author      : Seb
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "time_meas.h"

char cpun[] = "Undefined";
char *pcpun;

int main(int argc, char *argv[]) {
	volatile int i;
	volatile int j;
	unsigned long long acc;

	pcpun = cpun;
    if(argc > 1) {
    	pcpun = argv[1];
    }

	while(1) {
		get_cpu_freq_GHz();
		printf("Core: %s - CPU Freq = %f Ghz\n", pcpun, cpu_freq_GHz);
	}
	return EXIT_SUCCESS;
}
