/*
 ============================================================================
 Name        : vielearbeit.c
 Author      : Seb
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "time_meas.h"

int main(void) {
	volatile int i;

	while(1) {
		i++;
		get_cpu_freq_GHz();
	}
	return EXIT_SUCCESS;
}
